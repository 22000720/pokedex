//
//  PokedexList.swift
//  pokedex
//
//  Created by COTEMIG on 02/06/22.
//

import UIKit

class PokedexList: UIViewController, UITableViewDataSource {
    struct pokemon {
        let name: String;
        let types : Array<String>;
    }
    var pokemon_array : Array<pokemon> = [];
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 151
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "celula", for: indexPath) as? PokemonCelula {
            return cell;
        }
        return UITableViewCell()
    }
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self;
        self.tableView.rowHeight = 100
        getAllPokeData();
    }
    
    private func getAllPokeData() {
        let FM = FileManager.default;
        let path = FM.currentDirectoryPath;
        print(path)
        do{
            let itens = try FM.contentsOfDirectory(atPath: path + "home");
            for item in itens{
                print(item);
            }
        }
        catch{
            print("Error while reading directory! ");
        }
    }
}
