import os, requests, json

def getImage(url : str = "", pokemon_name : str = "") -> None:
    img_request = requests.get(url).content
    os.chdir("./img")
    with open(f"{pokemon_name}.png", "wb+") as image:
        image.write(img_request)
        image.close
    os.chdir("../")

def writePokeData(url : str = "") -> None:
    base_request = requests.get(url).text
    json_pokemon = json.loads(base_request)
    pokemon_name = json_pokemon["name"]
    pokemon_types = [x["type"]["name"] for x in json_pokemon["types"]]
    pokemon_sprite = json_pokemon["sprites"]["front_default"]
    getImage(pokemon_sprite, pokemon_name)
    dict_pokemon = {"name": pokemon_name, "types": pokemon_types}
    os.chdir("json_data")
    with open(f"{pokemon_name}.json", "w") as json_data:
        json_data.write(json.dumps(dict_pokemon))
        json_data.close()
    os.chdir("../")

def getDiffTypes(url : str = "", list_types : list = []) -> None:
    base_request = requests.get(url).text
    json_pokemon = json.loads(base_request)
    pokemon_types = [x["type"]["name"] for x in json_pokemon["types"]]
    for x in pokemon_types:
        if x not in list_types:
            list_types.append(x)

def main() -> None:
    base_request = requests.get("https://pokeapi.co/api/v2/pokemon/?limit=151").text
    json_first_gen = json.loads(base_request) 
    poke_url = [x["url"] for x in json_first_gen["results"]]
    types = []
    for url in poke_url:
        getDiffTypes(url, types)
    print(types)

if __name__ == "__main__":
    main()

# types: ['grass', 'poison', 'fire', 'flying', 'water', 'bug', 'normal', 'electric', 'ground', 'fairy', 'fighting', 'psychic', 'rock', 'steel', 'ice', 'ghost', 'dragon']
